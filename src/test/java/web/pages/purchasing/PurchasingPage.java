package web.pages.purchasing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import web.setup.SetupWeb;
import web.utils.Utils;

public class PurchasingPage extends SetupWeb {

    public PurchasingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = "//div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/a") protected WebElement newStockItemLink;
    @FindBy (id = "Input_ItemName") protected WebElement inputItemName;
    @FindBy (css = ".btn-primary") protected WebElement saveButton;
    @FindBy (xpath = "//form/div[4]/button[2]") protected WebElement deleteButton;
    @FindBy (css = ".feedback-message-text") protected WebElement feedbackDeleteMessage;


    public void accessPurchasingPage() {
        driver.get(urlPurchasingPage);
    }

    public void createStockItem(String productName) {
        wait = new WebDriverWait(driver, 5);

        newStockItemLink.click();
        wait.until(ExpectedConditions.visibilityOf(inputItemName));
        inputItemName.sendKeys(productName);
        saveButton.click();
        Utils.log("Stock item created!");
    }

    public void deleteStockItem(String productName) throws InterruptedException {
        Utils.wait(3);
        for (int i = 1; i < 5; i++) {
            String nome = driver.findElement(By.xpath("//table/tbody/tr["+ i +"]/td[1]/div/a/span")).getText();
            if(nome.equals(productName)){
                driver.findElement(By.xpath("//table/tbody/tr["+ i +"]/td[1]/div/a/span")).click();
            }
        }
        wait.until(ExpectedConditions.visibilityOf(inputItemName));
        deleteButton.click();
        wait.until(ExpectedConditions.textToBePresentInElement(feedbackDeleteMessage, "Smart TV 46\" LG deleted"));
        Assert.assertEquals(feedbackDeleteMessage.getText(),"Smart TV 46\" LG deleted");
        Utils.wait(3);
        Utils.log("Stock item deleted!");

    }
}
