package web.pages.storekeeping;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import web.setup.SetupWeb;
import web.utils.Utils;

public class StorekeepingDetailsPage extends SetupWeb {

    public StorekeepingDetailsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (id = "Input_ItemName") protected WebElement itemNameElement;
    @FindBy (id = "Input_ItemName2") protected WebElement itemCodeElement;
    @FindBy (id = "Dropdown2") protected WebElement mainGroupElement;
    @FindBy (id = "Dropdown1") protected WebElement subGroupElement;


    public void validateProductInformation(String itemName, String itemCode, String mainGroup, String subGroup) {
        WebElement selectMainGroupElement = driver.findElement(By.id("Dropdown2"));
        Select mainGroupElement = new Select(selectMainGroupElement);

        WebElement selectSubGroupElement = driver.findElement(By.id("Dropdown1"));
        Select subGroupElement = new Select(selectSubGroupElement);

        Assert.assertEquals(itemNameElement.getAttribute("value"),"Smart TV 55\"");
        Utils.log("Item name ok!");
        Assert.assertEquals(itemCodeElement.getAttribute("value"),"1");
        Utils.log("Item code ok!");
        Assert.assertEquals(mainGroupElement.getAllSelectedOptions().get(0).getText(),"Eletrônicos");
        Utils.log("Main group ok!");
        Assert.assertEquals(subGroupElement.getAllSelectedOptions().get(0).getText(),"TV");
        Utils.log("Sub group ok");
    }
}
