package web.pages.storekeeping;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import web.setup.SetupWeb;
import web.utils.Utils;

public class StorekeepingPage extends SetupWeb {

    public StorekeepingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (id = "Input_SearchItemName") protected WebElement searchItemName;
    @FindBy (xpath = "//tbody/tr/td[1]/div/a/span") protected WebElement searchItemNameResult;


    public void productSearch(String product) {
        searchItemName.sendKeys(product);
    }

    public void validateFilter(String product) throws InterruptedException {
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.textToBePresentInElement(searchItemNameResult, product));
        Assert.assertEquals(product,searchItemNameResult.getText());
        Utils.log("Product validated!");
        Utils.wait(5);
    }

    public void accessStoreKeepingPage() {
        driver.get(urlStorekeepingpPage);
    }

    public void accessProduct(String product) throws InterruptedException {
        searchItemName.sendKeys(product);
        searchItemNameResult.click();
        Utils.wait(3);
    }
}
