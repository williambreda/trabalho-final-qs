package web.setup;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import web.pages.purchasing.PurchasingPage;
import web.pages.storekeeping.StorekeepingDetailsPage;
import web.pages.storekeeping.StorekeepingPage;

import java.util.concurrent.TimeUnit;

public class SetupWeb {

    //pages
    protected StorekeepingPage storekeepingPage;
    protected StorekeepingDetailsPage storekeepingDetailsPage;
    protected PurchasingPage purchasingPage;

    //driver
    protected WebDriver driver;
    protected WebDriverWait wait;

    //pages
    protected final String baseURL = "https://william-colombobreda.outsystemscloud.com";
    protected final String urlRetailRoundupPage = baseURL + "/RetailRoundup/";
    protected final String urlStorekeepingpPage = baseURL + "/RetailRoundup/Storekeeping";
    protected final String urlPurchasingPage = baseURL + "/RetailRoundup/Purchasing";
    protected final String urlAdministrationPage = baseURL + "/RetailRoundup/Administration";
    protected final String urlReportsPage = baseURL + "/RetailRoundup/Reports";


    @BeforeClass
    public void setUpClass(){
        System.setProperty("webdriver.chrome.driver", "C:\\driver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--test-type");
        //chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--window-size=1366,768");

        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(urlRetailRoundupPage);
    }


    @AfterClass
    public void tearDownClass(){
        driver.close();
    }

}
