package web.test;

import org.testng.annotations.Test;
import web.pages.storekeeping.StorekeepingDetailsPage;
import web.pages.storekeeping.StorekeepingPage;
import web.setup.SetupWeb;

public class StorekeepingTest extends SetupWeb {


    @Test(groups = {"web"}, priority = 1)
    public void searchItemName() throws InterruptedException {
        storekeepingPage = new StorekeepingPage(driver);

        storekeepingPage.accessStoreKeepingPage();
        storekeepingPage.productSearch("Smart TV 55\"");
        storekeepingPage.validateFilter("Smart TV 55\"");
    }

    @Test(groups = {"web"}, priority = 1)
    public void validateProductInformation() throws InterruptedException {
        storekeepingPage = new StorekeepingPage(driver);
        storekeepingDetailsPage = new StorekeepingDetailsPage(driver);

        storekeepingPage.accessStoreKeepingPage();
        storekeepingPage.accessProduct("Smart TV 55\"");
        storekeepingDetailsPage.validateProductInformation("Smart TV 55\"","1","Eletrônicos","TV");
    }
}
