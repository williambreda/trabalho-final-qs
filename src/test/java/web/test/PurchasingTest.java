package web.test;

import org.testng.annotations.Test;
import web.pages.purchasing.PurchasingPage;
import web.pages.storekeeping.StorekeepingPage;
import web.setup.SetupWeb;

public class PurchasingTest extends SetupWeb {

    @Test(groups = {"web"}, priority = 1)
    public void addDeleteStockItem() throws InterruptedException {
        purchasingPage = new PurchasingPage(driver);

        purchasingPage.accessPurchasingPage();
        purchasingPage.createStockItem("Smart TV 46\" LG");
        purchasingPage.deleteStockItem("Smart TV 46\" LG");
    }
}
