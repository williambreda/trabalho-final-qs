package web.utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {



    public static String getHoraMinuto(){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static String getDiaHoraMinuto(){

        SimpleDateFormat sdf = new SimpleDateFormat("ddHHmm");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static String getHoraMinutoSegundo(){

        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }


    public static String getAnoDiaMes(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static String getDiaMesHoraMinuto(){

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM HH:mm");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static String getDay(String pattern){

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static void log(String log){
        System.out.println("STEP: " + log);
    }

    public static void log(int log){
        System.out.println("STEP: " + log);
    }

    public static void getScreenshot(WebDriver driver, boolean erro, String testName){

       File diretorioDestino = new File("src/test/resources/screenshots");
       File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
       scrFile.renameTo(new File(diretorioDestino, testName + getDiaHoraMinuto() + ".png"));

    }

    public static void wait(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }

    public static void calculaTempoImport (String inicio, String fim) throws ParseException {

        Date date1 = new SimpleDateFormat("ss").parse(inicio);
        Date date2 = new SimpleDateFormat("ss").parse(fim);

        long diff = (date1.getTime() - date2.getTime());
        Utils.log("Tempo do import: " + ((diff / 1000) * 0.6) + " segundos.");

    }

    public static void sendKeys(String textToBeTyped) throws AWTException, InterruptedException {
        String text = textToBeTyped;
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);
        wait(1);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        wait(1);
        robot.keyPress(KeyEvent.VK_ENTER);
        wait(1);
    }


}
